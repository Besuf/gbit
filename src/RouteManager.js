import React from "react";
import { Switch, Redirect, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import OrderPage from "./pages/OrderPage";

export default function RouteManager() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/order" component={OrderPage} />
      <Redirect to="/" />
    </Switch>
  );
}
