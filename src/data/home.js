export const assets = [
  {
    imgSrc: "/images/assets/ai.jpg",
    label: "Dynamic Bone",
    status: "FREE",
  },
  {
    imgSrc: "/images/assets/scene.jpg",
    label: "Wonderful game scene",
    status: "PAID",
  },
  {
    imgSrc: "/images/assets/env.jpeg",
    label: "Island Environment",
    status: "FREE",
  },
  {
    imgSrc: "/images/assets/gojo.jpg",
    label: "Ethiopian Hut",
    status: "PAID",
  },
];

export const categories = [
  "3D",
  "2D",
  "TOOLS",
  "NATIVE",
  "SOUND EFFECTS",
  "SHORT",
];

export const slides = [
  {
    quote: "Make Your Dream a Reality !",
    image: "/images/slide/unreal.jpg",
  },
  {
    quote: "Lookout For Technologies Outhere!",
    image: "/images/slide/godot.jpg",
  },
  {
    quote: "And The Result Will Impress You !",
    image: "/images/slide/godotWork.jpg",
  },
];
