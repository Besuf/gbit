import React from "react";
import Container from "@material-ui/core/Container";
import Order from "../features/order/Order";

export default function OrderPage() {
  return (
    <Container>
      <Order />
    </Container>
  );
}
