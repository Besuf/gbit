import React from "react";
import Home from "../features/home/Home";
import Container from "@material-ui/core/Container";

export default function HomePage() {
  return (
    <Container maxWidth="lg">
      <Home />
    </Container>
  );
}
