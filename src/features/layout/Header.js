import React from "react";
import AppBar from "@material-ui/core/AppBar";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Search from "./search/Search";
import { Link } from "react-router-dom";
import Hidden from "@material-ui/core/Hidden";
import GbitLogo from "./GbitLogo";
import MenuIcon from "@material-ui/icons/Menu";

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.secondary.main,

    padding: "1em 3em",
    [theme.breakpoints.down("md")]: {
      padding: "1em 1.5em",
    },
  },
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },

  links: {
    display: "flex",
    [theme.breakpoints.down("md")]: {
      justifyContent: "flex-end",
    },
  },
  link: {
    listStyle: "none",
    color: theme.palette.primary.main,
    marginLeft: "2em",
    fontSize: "1.6em",
    fontWeight: 500,
  },
  searchHolder: {},
  navigation: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      display: "block",
    },
    [theme.breakpoints.down("xs")]: {
      display: "flex",
      justifyContent: "flex-end",
      width: "70%",
    },
  },
}));

export default function Header() {
  const classes = useStyles();
  return (
    <div>
      <AppBar className={classes.root} position="relative">
        <Container maxWidth="xl" className={classes.container}>
          <GbitLogo />

          <div className={classes.navigation}>
            <Hidden xsDown>
              <>
                <Search />

                <ul className={classes.links}>
                  <li className={classes.link}>
                    <Link to="/order">Assets</Link>
                  </li>
                  <li className={classes.link}>
                    <Link to="/news">News</Link>
                  </li>
                  <li className={classes.link}>
                    <Link to="/order">Community</Link>
                  </li>
                </ul>
              </>
            </Hidden>
            <Hidden implementation="css" smUp>
              <MenuIcon fontSize="large" color="primary" />
            </Hidden>
          </div>
        </Container>
      </AppBar>
    </div>
  );
}
