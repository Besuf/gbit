import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import GbitLogo from "./GbitLogo";
import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.secondary.light,
    padding: "4em 8em",
    paddingBottom: "1em",
    color: "#fff",
  },
  link: {
    display: "block",
    fontSize: "1.1em",
  },
  copyRight: {
    marginTop: "2.5em",
    fontSize: "0.9em",
  },
}));

export default function Footer() {
  const classes = useStyles();
  return (
    <Container className={classes.root} maxWidth="xl">
      <Grid container alignItems="center">
        <Grid item sm={3} lg={4}>
          <GbitLogo />
        </Grid>
        <Grid item sm={2} lg={2} className={classes.linkGrid}>
          <Link to="/order" className={classes.link}>
            <a>Assets</a>
          </Link>
          <Link to="/order" className={classes.link}>
            <a>Community</a>
          </Link>
          <Link to="/" className={classes.link}>
            <a>News</a>
          </Link>
          <Link to="/order" className={classes.link}>
            <a>Order Asset</a>
          </Link>
        </Grid>
        <Grid item sm={2} lg={2} className={classes.linkGrid}>
          <Link to="/" className={classes.link}>
            <a>Blender Models</a>
          </Link>
          <Link to="/order" className={classes.link}>
            <a>Scenes</a>
          </Link>
          <Link to="/" className={classes.link}>
            <a>Bones</a>
          </Link>
          <Link to="/" className={classes.link}>
            <a>3D</a>
          </Link>
        </Grid>
        <Grid item sm={2} lg={2} className={classes.linkGrid}>
          <Link to="/order" className={classes.link}>
            <a>2D</a>
          </Link>
          <Link to="/" className={classes.link}>
            <a>Tools</a>
          </Link>
          <Link to="/" className={classes.link}>
            <a>Native</a>
          </Link>
          <Link to="/" className={classes.link}>
            <a>Sound Effects</a>
          </Link>
          <Link to="/" className={classes.link}>
            <a>Short</a>
          </Link>
        </Grid>
        <Grid item sm={3} lg={2} className={classes.linkGrid}>
          <Link to="/" className={classes.link}>
            <a>Email: mail@gbit.com</a>
          </Link>
          <Link to="/" className={classes.link}>
            <a>Phone: +251923479881</a>
          </Link>
        </Grid>
      </Grid>
      <Typography align="center" className={classes.copyRight}>
        Copyright 2021
      </Typography>
    </Container>
  );
}
