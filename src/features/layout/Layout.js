import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Footer from "./Footer";
import Header from "./Header";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
  },
  upperWrapper: {
    flexGrow: 1,
  },
}));
export default function Layout(props) {
  const classes = useStyles();
  const { children } = props;
  return (
    <div className={classes.root}>
      <div className={classes.upperWrapper}>
        <Header />
        {children}
      </div>
      <Footer />
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.any,
};
