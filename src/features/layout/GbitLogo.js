import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  logo: {
    color: theme.palette.primary.main,
    cursor: "pointer",
    minWidth: "15%",
    fontFamily: "Orbitron",
    [theme.breakpoints.down("lg")]: {
      minWidth: "25%",
    },
    [theme.breakpoints.down("md")]: {
      minWidth: "30%",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.7em",
    },
  },
  logoName: {
    padding: 0,
    margin: 0,
    fontFamily: "Orbitron",
    fontSize: "3em",
    [theme.breakpoints.down("xs")]: {
      fontSize: "2.4em",
    },
  },
}));
export default function GbitLogo() {
  const classes = useStyles();
  return (
    <Link to="/">
      <div className={classes.logo}>
        <Typography variant="h1" className={classes.logoName}>
          G-Bit
        </Typography>
        Your Next Favorite Asset Store
      </div>
    </Link>
  );
}
