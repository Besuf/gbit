import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Carousel, { consts } from "react-elastic-carousel";
import IconButton from "@material-ui/core/IconButton";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { slides } from "../../data/home";
import Slide from "../../components/Slide";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow:
      "rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px",
    background: theme.palette.secondary.light,
    width: "100%",
    height: "350px",
    marginTop: theme.spacing(3),
    borderRadius: "1em",
  },
  carousel: {
    width: "100%",
    height: "100%",
  },
  carouselItem: {
    width: "100%",
    height: "350px",
  },
}));

export default function Jumbotron() {
  const classes = useStyles();
  function myArrow({ type, onClick, isEdge }) {
    const icon =
      type === consts.PREV ? (
        <ArrowBackIosIcon onClick={onClick} style={{ color: "#fff" }} />
      ) : (
        <ArrowForwardIosIcon onClick={onClick} style={{ color: "#fff" }} />
      );
    return (
      <div
        style={{
          height: "100%",
          display: "grid",
          placeItems: "center",
        }}
      >
        <IconButton disabled={isEdge} style={{ height: "3em", width: "3em" }}>
          {icon}
        </IconButton>
      </div>
    );
  }

  return (
    <div className={classes.root}>
      <Carousel
        className={classes.carousel}
        enableAutoPlay={true}
        autoPlaySpeed={6000}
        renderArrow={myArrow}
        pagination={false}
      >
        {slides.map((slide, index) => (
          <div className={classes.carouselItem} key={`Slide-${index}`}>
            <Slide data={slide} />
          </div>
        ))}
      </Carousel>
    </div>
  );
}
