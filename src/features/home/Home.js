import React from "react";
import Assets from "./Assets";
import Categories from "./Categories";
import Jumbotron from "./Jumbotron";

export default function Home() {
  return (
    <div>
      <Jumbotron />
      <Assets />
      <Categories />
    </div>
  );
}
