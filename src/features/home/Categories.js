import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Title from "../../components/Title";
import Grid from "@material-ui/core/Grid";
import { assets, categories } from "../../data/home";
import Asset from "../../components/Asset";
import Category from "../../components/Category";

const useStyles = makeStyles((theme) => ({
  root: {
    // padding: "2em 2em",
    paddingTop: "3em",
  },
  item: {
    padding: "2em",
  },
}));

export default function Categories() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Title>Browse By Categories</Title>
      <Grid container>
        {categories.map((cat, index) => (
          <Grid
            item
            key={`Asset-${index}`}
            className={classes.item}
            xs={12}
            sm={6}
            md={3}
          >
            <Category data={cat} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
}
