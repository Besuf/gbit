import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Title from "../../components/Title";
import Grid from "@material-ui/core/Grid";
import { assets } from "../../data/home";
import Asset from "../../components/Asset";

const useStyles = makeStyles((theme) => ({
  root: {
    // padding: "2em 2em",
    paddingTop: "3em",
  },
  item: {
    padding: "2em",
  },
}));

export default function Assets() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Title> New Assets</Title>
      <Grid container>
        {assets.map((asset, index) => (
          <Grid
            item
            key={`Asset-${index}`}
            className={classes.item}
            xs={12}
            sm={6}
            md={3}
          >
            <Asset data={asset} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
}
