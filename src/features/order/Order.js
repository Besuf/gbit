import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Title from "../../components/Title";
import {
  FormControl,
  Grid,
  InputLabel,
  Select,
  TextareaAutosize,
  Button,
  TextField,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  title: {
    fontSize: "1.5em",
    fontWeight: 600,
    color: "gray",
  },
  form: {
    marginTop: theme.spacing(2),
  },
  btn: {
    marginTop: "2em",
    color: theme.palette.primary.main,
  },
}));

export default function Order() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography className={classes.title}>
        Didn't find what you are looking for?
      </Typography>
      <br />
      <Title>Order Asset</Title>
      <form className={classes.form}>
        <Grid container spacing={3}>
          <Grid item md={8}>
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="category">Category</InputLabel>
              <Select fullWidth label="Category" id="category" value="3d">
                <option value="3d">3D</option>
                <option value="2d">2D</option>
                <option value="tools">Tools</option>
                <option value="native">Native</option>
                <option value="soundeffects">Sound Effects</option>
              </Select>
            </FormControl>
          </Grid>
          <Grid item md={8}>
            <InputLabel style={{ marginBottom: "1em" }}>
              Asset Description
            </InputLabel>
            <FormControl fullWidth>
              <TextareaAutosize id="description" rowsMin={9} />
            </FormControl>
          </Grid>
          <Grid item md={8}>
            <InputLabel style={{ marginBottom: "1em" }}>
              Sample Sketch (Optional)
            </InputLabel>

            <Button>../Upload</Button>
          </Grid>
          <Grid item md={4}></Grid>
          <Grid item md={4}>
            <TextField
              name="budget"
              label="Maximum Budget"
              fullWidth
              variant="outlined"
            />
          </Grid>
          <Grid item md={4}>
            <TextField
              name="deadline"
              label="DeadLine"
              fullWidth
              variant="outlined"
            />
          </Grid>
        </Grid>
        <Button
          variant="contained"
          color="secondary"
          size="large"
          className={classes.btn}
        >
          Post Order
        </Button>
      </form>
    </div>
  );
}
