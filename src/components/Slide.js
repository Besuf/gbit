import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
    width: "100%",
    position: "relative",
    paddingLeft: "3em",
    paddingTop: "8em",
  },
  imageHolder: {
    position: "absolute",
    height: "100%",
    right: 0,
    top: 0,
    width: "60%",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },
  img: {
    objectFit: "cover",
    height: "100%",
    width: "100%",
  },
  overlay: {
    background:
      "linear-gradient(90deg, rgba(42,42,42,0.7) 15%, rgba(42,42,42,0.55) 52%, rgba(63,63,63,0.3) 84%, rgba(255,255,255,0) 100%)",
    height: "100%",
    width: "100%",
    position: "absolute",
    left: 0,
    right: 0,
    zIndex: 2,
  },
  motivateText: {
    position: "relative",
    zIndex: 3,
    color: "#fff",
    fontSize: "2.5em",
    fontFamily: "Orbitron",
    width: "35%",
    [theme.breakpoints.down("sm")]: {
      width: "70%",
      fontSize: "2.5em",
    },
    [theme.breakpoints.down("xs")]: {
      width: "100%",
      fontSize: "1.5em",
    },
    // background: "red",
  },
}));

export default function Slide(props) {
  const classes = useStyles();
  const { data } = props;
  return (
    <div className={classes.root}>
      <Typography className={classes.motivateText}>{data.quote}</Typography>
      <div className={classes.imageHolder}>
        <div className={classes.overlay}></div>
        <img src={data.image} className={classes.img} />
      </div>
    </div>
  );
}
