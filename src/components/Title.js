import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: "2.5em",
    fontWeight: 700,
  },
}));

export default function Title(props) {
  const classes = useStyles();
  const { children } = props;
  return (
    <Typography variant="h2" className={classes.title}>
      {children}
    </Typography>
  );
}
