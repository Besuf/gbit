import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    width: "100%",
    height: "250px",
    boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
    borderRadius: "1em",
    display: "grid",
    placeItems: "center",
    padding: "3em",
    cursor: "pointer",
  },
  cat: {
    fontSize: "2.1em",
    fontWeight: 800,
    color: "theme.palette.secondary.light",
    fontFamily: "Orbitron",
  },
}));

export default function Category(props) {
  const { data } = props;
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography className={classes.cat} align="center">
        {data}
      </Typography>
    </div>
  );
}
