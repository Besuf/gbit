import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    width: "100%",
    height: "250px",
    boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
    borderRadius: "1em",
    cursor: "pointer",
  },
  img: {
    objectFit: "cover",
    height: "100%",
    width: "100%",
    borderRadius: "0.5em",
  },
  subLabel: {
    position: "absolute",
    padding: "2em",
    left: 0,
    bottom: 0,
    width: "100%",
    background: "rgba(0,0,0,0.6)",
    borderRadius: "0.5em",
  },
  label: {
    color: "#fff",
    fontWeight: "600",
    fontSize: "1.4em",
  },
  status: {
    position: "absolute",
    top: 0,
    right: 0,
    padding: "0.4em 1em",
    fontWeight: 700,
    textAlign: "center",
  },
  free: {
    background: theme.palette.success.light,
    color: "#fff",
  },
  paid: {
    background: theme.palette.primary.main,
    color: "#000",
  },
}));

export default function Asset(props) {
  const { data } = props;
  const classes = useStyles();
  const [hovered, setHovered] = useState(false);
  return (
    <div
      className={classes.root}
      onMouseOver={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      <img src={data.imgSrc} className={classes.img} />
      {hovered && (
        <div className={classes.subLabel}>
          <Typography align="center" className={classes.label}>
            {data.label}
          </Typography>
        </div>
      )}
      <div
        className={
          data.status === "FREE"
            ? clsx(classes.status, classes.free)
            : clsx(classes.status, classes.paid)
        }
      >
        {data.status === "FREE" ? "FREE" : "PAID"}
      </div>
    </div>
  );
}
