import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from "./utils/theme";
import RouteManager from "./RouteManager";
import { BrowserRouter } from "react-router-dom";
import Layout from "./features/layout/Layout";

function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Layout>
          <RouteManager />
        </Layout>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
