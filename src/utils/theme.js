import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#ffcc66",
    },
    secondary: {
      main: "#010101",
      light: "#2a2a2a",
    },
    background: {
      default: "#fff",
      paper: "#eeeeee",
    },
  },

  typography: {
    fontFamily: "Titillium Web, sans-serif",
  },
});

export default theme;
